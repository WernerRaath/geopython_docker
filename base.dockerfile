ARG PYTHON_VER=3.6
FROM python:${PYTHON_VER}-alpine3.9

ENV PYTHONPATH=/usr/local/lib/python${PYTHON_VER}/site-packages

#
# The latest alpine images on docker hub is behind alpine-edge
# So patch /etc/apk/repositories to get postgis, etc.
#
RUN echo "@edge http://nl.alpinelinux.org/alpine/edge/main" >>/etc/apk/repositories \
    && echo "@edgecommunity http://nl.alpinelinux.org/alpine/edge/community" >>/etc/apk/repositories \
    && echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >>/etc/apk/repositories \
    && apk update \
    && apk upgrade apk-tools \
    && apk upgrade --update-cache --available \
    && echo "ipv6" >> /etc/modules \
    && apk add --update \
        linux-headers \
        build-base \
        #   Required for pip numpy package
        libxml2 \
        openblas-dev \
        py-psycopg2 postgresql-dev libffi-dev

# # Include /usr/bin to use correct python3
# # Base image uses /usr/local/bin/python3 which can't find python3-* modules
 ENV PATH=/usr/bin:$PATH
