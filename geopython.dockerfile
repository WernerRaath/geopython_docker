FROM registry.gitlab.com/wernerraath/geopython_docker/base:master

ARG GEOS_VERSION=3.6.1
ARG PROJ_VERSION=4.9.3
ARG PROJ_DATUMGRID_VERSION=1.6
ARG GDAL_VERSION=2.2.0

#
# The latest alpine images on docker hub is behind alpine-edge
# So patch /etc/apk/repositories to get postgis, etc.
#
RUN cd /tmp \
    && wget http://download.osgeo.org/geos/geos-${GEOS_VERSION}.tar.bz2 \
    && tar xjf geos-${GEOS_VERSION}.tar.bz2 \
    && cd geos-${GEOS_VERSION} \
    && ./configure --enable-silent-rules CFLAGS="-D__sun -D__GNUC__"  CXXFLAGS="-D__GNUC___ -D__sun" --with-python \
    && make -s -j4 \
    && make -s install \
    && cd /tmp \
    && rm geos-${GEOS_VERSION}.tar.bz2 \
    && rm -R geos-${GEOS_VERSION} \
    # Installing PROJ dependency
    && wget http://download.osgeo.org/proj/proj-${PROJ_VERSION}.tar.gz \
    && wget http://download.osgeo.org/proj/proj-datumgrid-${PROJ_DATUMGRID_VERSION}.zip \
    && tar xzf proj-${PROJ_VERSION}.tar.gz \
    && cd proj-${PROJ_VERSION}/nad \
    && unzip ../../proj-datumgrid-${PROJ_DATUMGRID_VERSION}.zip \
    && cd .. \
    && ./configure --enable-silent-rules --with-python \
    && make -s -j4 \
    && make -s install \
    && cd /tmp \
    && rm proj-${PROJ_VERSION}.tar.gz \
    && rm proj-datumgrid-${PROJ_DATUMGRID_VERSION}.zip \
    && rm -R proj-${PROJ_VERSION} \
    # Installing GDAL
    && wget http://download.osgeo.org/gdal/${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz \
    && tar xzf gdal-${GDAL_VERSION}.tar.gz \
    && cd gdal-${GDAL_VERSION} \
    && ./configure --enable-silent-rules --with-static-proj4=/usr/local/lib --with-python --with-threads --with-pg=/usr/bin/pg_config \
    && make -s -j4 \
    && make -s install \
    && cd /tmp \
    && rm gdal-${GDAL_VERSION}.tar.gz \
    && rm -R gdal-${GDAL_VERSION} \
    && python -c "from osgeo import gdal, ogr, osr" \
    && ogr2ogr --formats | grep PostgreSQL
